\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}
\usepackage{multimedia}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Classificação}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Universidade Presbiteriana Mackenzie}

\date{\today} % Date, can be changed to a custom date
%\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\begin{frame}
    \frametitle{Classificação: definição}
    \begin{definition}
        Dado um conjunto de registros (\alert{dados de treinamento}) \cite{smu}:
        \begin{itemize}
            \item Cada um dos registros possui um conjunto de atributos, sendo um deles uma \alert{classe};
            \item Encontre um \alert{modelo} para a classe como uma função dos valores dos outros atributos;
            \item \alert{Objetivo}: novos registros devem ser atribuídos a uma classe da forma mais precisa possível.
        \end{itemize}
        Um \alert{conjunto de dados de teste} é utilizado para determinar a acurácia do modelo. Normalmente os dados são divididos entre teste e treinamento, sendo os dados de treinamento utilizados para construir o modelo e os dados de testes para validar.
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Tarefa de classificação}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/classification-task}
		\label{fig:classification-task}
		\caption{Abordagem geral para construir uma tarefa de classificação \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Exemplos de tarefas de classificação}
    \begin{itemize}
        \item Predizer se um tumor é benigno ou maligno;
        \item Classificação de transações de cartão de crédito como fraudulentas ou legítimas;
        \item Classificar estruturas secundárias de proteínas (\emph{alpha-helix}, \emph{beta-sheet}, ou \emph{random coil}).
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \begin{minipage}[ht]{0.59\textwidth}
            \includegraphics[width=.8\textwidth]{../imagens/classification-ex01}
        \end{minipage}
        \begin{minipage}[ht]{0.39\textwidth}
            \includegraphics[width=1\textwidth]{../imagens/classification-ex02}
        \end{minipage}
		\label{fig:classification-examples}
		\caption{Exemplos de tarefas de classificação \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Conteúdo seguro}
    \begin{itemize}
        \item Uma grande quantidade da receita em conteúdos digitais vem da publicidade (\emph{advertising});
        \item As marcas não querem ser associadas ao que consideram \alert{conteúdo impróprio};
        \item O conceito de impróprio \alert{depende da marca};
        \item Existem algumas regras gerais.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/youtube-bs}
		\label{fig:youtube-bs}
		\caption{Política de \emph{brand safety} para vídeos do YouTube\footnote{\url{https://support.google.com/youtube/answer/6162278}}}
    \end{figure}
\end{frame}


\section{Avaliação de modelos}

\begin{frame}
    \frametitle{Avaliação de modelos}
    \begin{minipage}[ht]{0.64\textwidth}
        \alert{Pergunta central} \cite{mannheim}: qual a capacidade do modelo em classificar novas amostras? (generalização de performance)
    \end{minipage}
    \begin{minipage}[ht]{0.34\textwidth}
        \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=1\textwidth]{../imagens/classification-unseen}
			\label{fig:classification-unseen}
	    \end{figure}
    \end{minipage}
    \begin{itemize}
        \item Métricas para avaliação do modelo: como medir a performance de um modelo?
        \item Métodos para avaliação do modelo: como obter estimativas confiáveis?
    \end{itemize}
\end{frame}

\subsection{Métricas para avaliação do modelo}

\begin{frame}
    \frametitle{Métricas para avaliação}
    \begin{itemize}
        \item Foco na \alert{capacidade preditiva} do modelo, ao invés do tempo para classificar ou construir novos modelos;
        \item A matriz de confusão conta as classificações corretas e falsas, que são a base para o cálculo de outras métricas de performance.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/confusion-matrix}
    	\label{fig:confusion-matrix}
    	\caption{Exemplo de matriz de confusão \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Acurácia e taxa de erros}
    A acurácia leva em consideração o número de predições corretas entre o total de predições realizadas.
    \[
        \begin{split}
            accuracy &= \frac{TP + TN}{TP + FP + FN + TN} = \frac{\text{predições corretas}}{\text{todas as predições}} \\
            \text{error rate} &= 1 - accuracy
        \end{split}
    \]
    \begin{figure}[ht!]
        \begin{minipage}[ht]{0.49\textwidth}
	        \centering
	        \includegraphics[width=.8\textwidth]{../imagens/accuracy-example}
	    \end{minipage}
	    \begin{minipage}[ht]{0.49\textwidth}
	        \[
	            accuracy = \frac{25+15}{25+15+6+4} = 0,80
	        \]
        \end{minipage}
    	\label{fig:accuracy-example}
    	\caption{Exemplo de cálculo de acurácia \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Problema do desbalanceamento de classes}
    \begin{itemize}
        \item Existem casos onde as classes possuem \alert{frequências desiguais}:
        \begin{itemize}
            \item Detecção de fraudes: 98\% das transações são lícitas, somente 2\% são fraudulentas;
            \item e-commerce: 99\% dos usuários não compram, somente 1\% compram (conversão);
            \item Detecção de invasores: 99,99\% dos usuários não são invasores;
            \item Criminalidade: 99,99\% dos cidadãos não são criminosos.
        \end{itemize}
        \item A classe de interesse é normalmente chamada de \alert{positiva}, enquanto as outras são negativas;
        \item Considere um problema de duas classes:
        \begin{itemize}
            \item Exemplos negativos: 9990;
            \item Exemplos positivos: 10;
            \item Se o modelo predizer que todas as amostras pertencem à classe negativa a acurácia é $\frac{9990}{10000} = 99,9\%$
            \item A \alert{acurácia engana} porque o modelo não detecta nenhum exemplo positivo.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Precision e Recall}
    \begin{definition}
            As medidas Precision $P$ e Recall $R$ são definidas como:
            \[
                \begin{split}
                P &= \frac{TP}{TP + FP} \\
                R &= \frac{TP}{TP + FN}
                \end{split}
            \]
            onde TP e FN são definidos de acordo com a Tabela \ref{tab:precision-recall}.
        \begin{table}[ht]
            \centering
	        \begin{tabular}{c|c|c}
	             & Classificações positivas & Classificações negativas \\
	             \hline
	             Positivos & True Positives (TP) & Falsos Positives (FP) \\
	             \hline
	             Negativos & False Negatives (FN) & True Negatives (TN) \\
	        \end{tabular}
	        \label{tab:precision-recall}
	        \caption{Classificadores para positivos e negativos \cite{cambridge}}
	    \end{table}
	    Observe que as medidas têm como foco a classe \alert{positiva}.
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Precision e Recall: visualização}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/precision-recall}
    	\label{fig:precision-recall}
    	\caption{Visualização da aplicação de Precision e Recall para avaliação de performance do classificador \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Precision e Recall: problema}
    Considere os dados da Tabela para o cálculo de Precision e Recall:
    \begin{table}[ht]
            \centering
	        \begin{tabular}{c|c|c}
	             & Classificações positivas & Classificações negativas \\
	             \hline
	             Positivos & 1 (TP) & 99 (FP) \\
	             \hline
	             Negativos & 0 (FN) & 100 (TN) \\
	        \end{tabular}
	        \label{tab:precision-recall-problem}
	        \caption{Problema de Precision e Recall \cite{smu}}
	    \end{table}
    \begin{itemize}
        \item A matriz de confusão apresenta:
        \begin{itemize}
            \item Precision $p = 100\%$
            \item Recall $r = 1\%$
        \end{itemize}
        \item Classificar somente um exemplo como positivo e nenhum como negativo traz esse problema;
        \item Assim, queremos uma medida que:
        \begin{enumerate}
            \item Combina Precision e Recall;
            \item Aumenta se ambos os valores crescem.
        \end{enumerate}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Calculando F}
    \begin{definition}
        Considere a hipótese nula $H_0$ como a classificação estar correta. O operador $F$ pode ser definido como:
        \[
            \begin{split}
                F &= \frac{1}{\alpha \frac{1}{P} + (1 - \alpha)\frac{1}{R} } = \frac{(\beta^2 + 1)PR}{\beta^2 P + R}
            \end{split}
        \]
        onde 
        \[
            \beta^2 = \frac{1 - \alpha}{\alpha}
        \]
        e $\alpha \in [0, 1]$ e $\beta^2 \in [0, \infty]$ 
        \begin{itemize}
            \item $\alpha$ é a probabilidade de rejeitar $H_0$ quando for verdadeira;
            \item $\beta$ é a probabilidade de aceitar $H_0$ quando for falsa.
        \end{itemize}
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Calculando $\beta$ e $\alpha$}
    \begin{itemize}
        \item \alert{Pergunta}: como definir os valores de $\beta$ e $\alpha$?
        \item A ideia é encontrar uma medida que represente a \alert{menor classe};
        \pause
        \item A \alert{média harmônica} entre dois números tende a ser mais próxima do menor deles.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.6\textwidth]{../imagens/harmonic-mean}
    	\label{fig:harmonic-mean}
    	\caption{Diferença entre média harmônica e média aritmética \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{$F_1$ Score}
    \begin{itemize}
        \item O operador $F$ é mais utilizado no seu formato \alert{balanceado}:
        \begin{itemize}
            \item $\beta = 1$ ou
            \item $\alpha = 0.5$
        \end{itemize}
        \item O formato balanceado representa a \alert{média harmônica} entre P e R.
    \end{itemize}
    \[
        \begin{split}
            \frac{1}{F_{\alpha = 0.5}} &= \frac{1}{2}(\frac{1}{P} + \frac{1}{R}) \\
            F_{\beta = 1} &= \frac{2PR}{P+R}
        \end{split}
    \]
    \begin{itemize}
        \item O operador $F_{\beta = 1}$ também é conhecido como \alert{$F_1$ Score}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Efeito do F1}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/f1-example}
    	\label{fig:f1-example}
    	\caption{Exemplo do efeito do F1 em classes desbalanceadas \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Gráfico do F1}
    Um balanceamento ótimo entre Precision e Recall está no topo do gráfico do F1.
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.6\textwidth]{../imagens/f1-optimal}
    	\label{fig:f1-optimal}
    	\caption{Medida ótima do F1 \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Matriz de custo}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/cost-matrix}
    	\label{fig:cost-matrix}
    	\caption{Cálculo da matriz de custos \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Calculando custo}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/calculando-custo}
    	\label{fig:calculando-custo}
    	\caption{Calculando o custo da classificação \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Custo vs acurácia}
    \begin{figure}[ht!]
        \begin{minipage}[ht]{0.59\textwidth}
	        \centering
	        \includegraphics[width=.8\textwidth]{../imagens/cost-accuracy}
        \end{minipage}
	    \begin{minipage}[ht]{0.39\textwidth}
    	    \scriptsize
	        A acurácia só é proporcional ao custo se:
	        \begin{enumerate}
	            \item $C(Yes|No)=C(No|Yes) = q$
	            \item $C(Yes|Yes)=C(No|No) = p$
	        \end{enumerate}
	        N = a + b + c + d \\
            accuracy = (a+d)/N
            \[
                \begin{split}
                    Cost &= p (a + d) + q (b + c) \\
                         &= p (a + d) + q (N - a - d) \\
                         &= q N - (q - p)(a + d) \\
                         &= N [q - (q-p) \times accuracy] 
                \end{split}
            \]
	    \end{minipage}
     	\label{fig:calculando-custo}
    	\caption{Comparativo custo vs acurácia \cite{smu}}    
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Curva ROC}
    \begin{itemize}
        \item Abordagem gráfica para mostrar a relação entre taxa de detecção e taxa de alarmes falsos;
        \item Alguns algoritmos de classificação fornecem um \alert{score de confiança}:
        \begin{itemize}
            \item Certeza que o algoritmo tem sobre a predição que foi feita;
            \item KNN: voto dos vizinhos (ou peso);
            \item Naive Bayes: probabilidade.
        \end{itemize}
        \item A curva ROC apresenta a \alert{taxa de positivos} e \alert{taxa de falsos positivos} em relação à confiança do algoritmo.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Desenhando a curva ROC}
    \begin{enumerate}
        \item Classifique os algoritmos de acordo com o \emph{score} de confiança;
        \item Passe por todas as classificações:
        \begin{itemize}
            \item Se a predição estiver certa: desenhe um passo para cima;
            \item Se a predição estiver errada: desenhe um passo para a direita.
        \end{itemize}
    \end{enumerate}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.6\textwidth]{../imagens/roc-curve}
    	\label{fig:roc-curve}
    	\caption{Exemplo de curva ROC vs confiança \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Interpretando curvas ROC}
	\begin{figure}
	    \begin{minipage}[ht]{0.59\textwidth}
	        \begin{itemize}
	            \item Quanto mais íngreme melhor:
	            \begin{itemize}
	                \item Escolhas aleatórias resultam na diagonal;
	                \item Um bom modelo deve produzir uma curva acima da diagonal.
	            \end{itemize}
	            \item Comparando modelos: se a curva A estiver acima da curva B o modelo A é melhor que o modelo B;
	            \item Medida para comparação de modelos: área sob a curva ROC (\emph{Area Under the Curve}) -- AUC.
	        \end{itemize}
	    \end{minipage}
  	    \begin{minipage}[ht]{0.39\textwidth}
	        \centering
	        \includegraphics[width=1\textwidth]{../imagens/roc-comparison}
	    \end{minipage}
    	\label{fig:roc-comparison}
    	\caption{Comparação de modelos através da curva ROC \cite{mannheim}}
	\end{figure}
\end{frame}

\subsection{Métodos para avaliação do modelo}

\begin{frame}
    \frametitle{Métodos para avaliação do modelo}
    \begin{itemize}
        \item Como obter uma \alert{estimativa confiável} da \alert{performance geral do modelo}?
        \item Abordagem geral: dividir o conjunto de dados em \alert{teste} e \alert{treinamento};
        \item Nunca teste em um modelo que foi utilizado para o treinamento!
        \begin{itemize}
            \item Como o modelo foi ajustado para os dados de treinamento, avaliar nos mesmos dados não gera uma estimativa confiável sobre a performance do modelo para novas amostras;
            \item É preciso separar de maneira definitiva os dados de teste e treinamento.
        \end{itemize}
        \item Quais registros usar para treinamento e para teste?
        \item Métodos para divisão dos dados:
        \begin{enumerate}
            \item Método \emph{holdout};
            \item Subamostragem aleatória;
            \item \emph{Cross validation}.
        \end{enumerate}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Curva de aprendizagem}
    \begin{minipage}[ht]{0.49\textwidth}
    	\begin{figure}
    	    \centering
	        \includegraphics[width=1\textwidth]{../imagens/learning-curve}
	    	\label{fig:learning-curve}
	    	\caption{Exemplo de curva de aprendizagem \cite{mannheim}}
		\end{figure}
    \end{minipage}
    \begin{minipage}[ht]{0.49\textwidth}
        \begin{itemize}
            \item A curva de aprendizagem mostra como a acurácia de acordo com o crescimento dos dados de treinamento;
            \item \alert{Conclusão}: se a performance do modelo estiver ruim, \alert{consiga mais dados para o treinamentio};
            \item Utilize dados classificados (\emph{labeled data}) tanto para teste quando para treinamento;
            \item \alert{Problema}: obter dados classificados normalmente é custoso porque envolve trabalho manual.
        \end{itemize}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Método holdout}
    \begin{itemize}
        \item O \alert{método holdout} reserva uma certa quantidade dos dados classificados para o teste e guarda o resto para treinamento;
        \item Normalmente separa 1/3 para teste e 2/3 para treinamento (ou 20\%/80\%)
        \item Para dados desbalanceados, amostras aleatórias podem não ser representativas;
        \item Poucos ou nenhum registro da classe minoritária nos dados de treino;
        \item \alert{Amostra estratificada}: coleta amostras independentes de cada classe, de forma que registros da classe minoritária estejam presentes em cada amostra.
    \end{itemize}
    \begin{figure}
    	\centering
        \includegraphics[width=.6\textwidth]{../imagens/holdout-example}
    	\label{fig:holdout-example}
    	\caption{Exemplo de amostra com holdout \cite{mannheim}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Subamostragem aleatória}
    \begin{itemize}
        \item Estimativas do houldout podem ser mais confiáveis repetindo o processo com diferentes amostras:
        \begin{itemize}
            \item A cada iteração uma certa proporção dos dados é selecionada \alert{aleatoriamente} para o treinamento;
            \item Calcula-se a \alert{média} das diferentes iterações.
        \end{itemize}
        \item Ainda não é ótimo, já que pode haver sobreposição de amostras:
        \begin{enumerate}
            \item Alguns \emph{outliers} podem sempre acabar nos dados de teste;
            \item Registros importantes para o treinamento podem aparecer nos dados de teste.
        \end{enumerate}
    \end{itemize}
    \begin{figure}
	    	\centering
	        \includegraphics[width=.7\textwidth]{../imagens/random-example}
	    	\label{fig:random-example}
	    	\caption{Exemplo de subamostragem aleatória \cite{mannheim}}
	\end{figure}
\end{frame}

\begin{frame}
    \frametitle{Cross validation}
    \begin{itemize}
        \item O método \emph{cross validation} evita a sobreposição de amostras:
        \begin{enumerate}
            \item os dados são divididos em $k$ subconjuntos de tamanhos iguais;
            \item cada subconjunto na vez é utilizado para teste e os restantes são enviados para treinamento;
            \item conhecido como \alert{k-fold cross validation}.
        \end{enumerate}
        \item Cada registro é utilizado apenas ma vez para teste;
        \item A média de performance e todas as execuções é a performance final do modelo;
        \item Medida frequente: k=10 (90\% treinamento, 10\% treinamento);
        \item Experimentos mostram que que k=10 é uma estimativa interessante para testar os dados e ainda assim utilizar a maior quantidade de dados possível para treinamento;
        \item Normalmente os subconjuntos são gerados utilizando amostragem estratificada para lidar com o \alert{desbalanceamento} das classes.
    \end{itemize}
\end{frame}

\section{Seleção de hiperparâmetros}


\begin{frame}
	\frametitle{Seleção de hiperparâmetros}
	\begin{itemize}
		\item Um \alert{hiperparâmetro} é um parâmetro que influencia o processo de aprendizagem, cujos valores são ajustados antes do início do processo:
		\begin{itemize}
			\item Ajustando \emph{thresholds} para árvores e regras de associação;
			\item Parâmetros \emph{gamma} e C para SVMs;
			\item Taxas de aprendizagem, camadas ocultas para ANNs.
		\end{itemize}
		\item Já os \alert{parâmetros} são obtidos a partir dos dados de treinamento, como  os pesos de uma ANN, probabilidade no Naive Bayes, ramos numa árvore, etc;
		\item Muitos métodos apresentam péssimos resultados com os hiperparâmetros nos seus valores padrão;
		\item Como escolher bons hiperparâmetros?
		\begin{itemize}
			\item Altere manualmente os valores e veja os resultados;
			\item Teste automaticamente diferentes valores (otimização de hiperparâmetros).
		\end{itemize}
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Otimização de hiperparâmetros}
	\begin{itemize}
		\item \alert{Objetivo}: encontrar a combinação de valores para os hiperparâmetros que resulta no modelo com menor erro geral;
		\item Como determinar as combinações de valores a serem testados?
		\begin{description}
			\item[Grid search] Teste todas as combinações possíveis em uma faixa de valores definidas pelo usuário;
			\item[Random search] Teste combinações aleatórias para os valores;
			\item[Evolutionary search] Mantenha valores específicos que funcionaram bem
		\end{description}
		\item É comum testar milhares de combinações diferentes para os valores (nuvem existe pra isso);
		\item \alert{Seleção de modelos}: de todos os modelos M aprendidos, selecione o modelo $m_{melhor}$ que parece generalizar melhor as amostras fornecidas.
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Seleção de modelos com dados de validação}
	\begin{minipage}[ht]{0.59\textwidth}
		\begin{enumerate}
			\item Divida os dados de treinamento $D_{train}$ em validação $D_{val}$ e treinamento $D_{tr}$;
			\item Aprenda os modelos $M$ em $D_{tr}$ utilizando diferentes combinações de valores de hiperparâmetros $P$;
			\item Selecione os melhores valores de parâmetro $p_{best}$ testando cada modelo $m_i$ nos dados de validação $D_{val}$;
			\item Construa o modelo final $m_{best}$ no conjunto de dados completo $D_{train}$ utilizando os valores $p_{best}$;
			\item Avalie $m_{best}$ no conjunto de dados de teste para obter uma estimativa sem viés da performance geral do modelo.
		\end{enumerate}
	\end{minipage}
	\begin{minipage}[ht]{0.39\textwidth}
		\begin{figure}
		    	\centering
		        \includegraphics[width=.9\textwidth]{../imagens/model-selection}
		    	\label{fig:model-selection}
		    	\caption{Separação de dados entre teste e treinamento \cite{mannheim}}
		\end{figure}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Utilização de cross validation para seleção do modelo}
	\begin{itemize}
		\item Contudo, ainda é desejável \cite{mannheim}:
		\begin{enumerate}
			\item Garantir que todos os exemplos sejam utilizados para validação pelo menos uma vez (importância de dados de classes não muito frequentes)
			\item Utilizar a maior quantidade possível de dados classificados para o treinamento (curva de aprendizagem)
		\end{enumerate}
		\item Utilização de  \emph{cross validation} atende ambas as premissas
	\end{itemize}
	\begin{figure}
	    	\centering
	    \includegraphics[width=.9\textwidth]{../imagens/cross-validation-model}
	    	\label{fig:cross-validation-model}
	    	\caption{Aplicação de \emph{k-fold cross validation} à seleção de modelos \cite{mannheim}}
	\end{figure}
\end{frame}

\begin{frame}
	\frametitle{Nested cross validation}
	\begin{minipage}[ht]{0.49\textwidth}
		\begin{itemize}
			\item Outer Cross-Validation:
			\begin{itemize}
				\item Estima o erro de generalização para $m_{best}$
				\item Os dados de treinamento são enviados para a camada interna (\emph{inner}) de cross validation a cada iteração
			\end{itemize}
			\item Inner cross-Validation:
			\begin{itemize}
				\item Busca a melhor combinação de parâmetros;
				\item Divide os dados de treinamento enviados pela camada externo em dados de treinamento e validação internos;
				\item Constrói o modelo $m_{best}$ utilizando os dados da camada externa.
			\end{itemize}
		\end{itemize}
	\end{minipage}
	\begin{minipage}[ht]{0.49\textwidth}
		Aninha dois \emph{loops} de \emph{cross-validation} um no outro.
		\begin{figure}
		    	\centering
		    \includegraphics[width=.9\textwidth]{../imagens/cross-validation-nested}
		    	\label{fig:cross-validation-nested}
		    	\caption{Exemplo de \emph{nested cross validation}  \cite{mannheim}}
		\end{figure}
	\end{minipage}
\end{frame}

\begin{frame}
	\frametitle{Seleção de features}
	\begin{itemize}
		\item Alguns métodos de classificação selecionam automaticamente o conjunto relevante de \emph{features} como parte do processo de aprendizagem (árvores de decisão, Random Forest, ANN, SVM);
		\item A performance dos outros métodos dependem do conjunto de \emph{features} fornecidas (KNN, Naive Bayes);
		\item Abordagens para seleção automática de \emph{features}:
		\begin{description}
			\item[Forward selection] Encontra o melhor valor para uma única \emph{feature}, adiciona outras, testa novamente.
			\item[Backward selection] Começa utilizando todas as \emph{features}, remove algumas, testa novamente
		\end{description}
		\item Utiliza \emph{nested cross-validation} para estimar o erro de generalização.
		\item Disciplina avançada: \alert{feature engineering} (pode ser etapa do pré-processamento).
	\end{itemize}
\end{frame}

\begin{frame}
	\frametitle{Sumário: seleção de hiperparâmetros e features}
	\begin{itemize}
		\item Seleção de hiperparâmetros:
		\begin{itemize}
			\item Padrão: \alert{sempre utilize otimização de hiperparâmetros};
			\item Se não for otimizado não é possível saber se um método não funciona.
		\end{itemize}
		\item Seleção de \emph{features}:
		\begin{itemize}
			\item Padrão: \alert{avalie as necessidades do método de classificação}
			\item Se for possível selecionar, dê preferência aos métodos automatizados.
		\end{itemize}
		\item Seleção de modelos
		\begin{itemize}
			\item Padrão: \alert{utilize nested cross-validation}
			\item Se o tempo de computação for grande demais tente obter \alert{mais recursos}. Se não for possível, tente \alert{otimizar os parâmetros} (diminuir os k-folds, número de parâmetros, tamanho da amostra, etc)
			\item Se a replicabilidade exata dos resultados for necessária, aplique a divisão somente em treino, teste e validação.
		\end{itemize}
		\item Se os dados estiverem desbalanceados, não esqueça de \alert{balancear os dados de treinamento}, não os dados de teste.
	\end{itemize}
\end{frame}


\section{Algoritmos de classificação}

\subsection{K-Nearest Neighbors}

\begin{frame}
    \frametitle{Problema exemplo}
    \begin{minipage}[ht]{0.49\textwidth}
	    \begin{itemize}
	        \item \alert{Problema}: desejamos realizar a previsão do tempo em determinado local;
	        \item Não existe nenhuma estação fornecendo informações sobre o tempo;
	        \item Vai chover ou fazer sol?
	        \item Como é possível prever o tempo?
	    \end{itemize}    
    \end{minipage}
    \begin{minipage}[ht]{0.49\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=1\textwidth]{../imagens/classification-forecast01}
			\label{fig:classification-forecast01}
			\caption{Exemplo de previsão do tempo \cite{mannheim}}
	    \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Ideia}
    \begin{minipage}[ht]{0.49\textwidth}
	    \begin{itemize}
	        \item Utilizamos a \alert{previsão do tempo média nas estações mais próximas}
	        \item Ex.:
	        \begin{itemize}
	            \item 3 cidades terão sol;
	            \item 2 cidades terão chuva;
	            \item Prevemos que \alert{não vai chover}.
	        \end{itemize}
	    \end{itemize}    
    \end{minipage}
    \begin{minipage}[ht]{0.49\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=.7\textwidth]{../imagens/classification-forecast02}
			\label{fig:classification-forecast02}
			\caption{Utilização de média previsão do tempo \cite{mannheim}}
	    \end{figure}
    \end{minipage}
    \begin{itemize}
        \item A utilização dos vizinhos mais próximos é chamada de \emph{K-nearest neighbors}, onde:
        \begin{itemize}
            \item K é o número de vizinhos que serão considerados;
            \item Para o exemplo da Figura, K=5;
            \item O conceito de ``próximo'' para o exemplo representa \alert{proximidade geográfica}.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Classificador KNN}
    \begin{minipage}[ht]{0.44\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=1\textwidth]{../imagens/knn-example}
			\label{fig:knn-example}
			\caption{Exemplo de classificador KNN \cite{mannheim}}
	    \end{figure}
    \end{minipage}
    \begin{minipage}[ht]{0.54\textwidth}
	    \begin{itemize}
	        \item O classificador KNN possui três requisitos:
	        \begin{itemize}
	            \item Um \alert{conjunto de registros};
	            \item Uma \alert{medida de distância} para calcular a distância entre os registros;
	            \item O \alert{valor de K}, ou seja, o número de vizinhos próximos a considerar.
	        \end{itemize}
          	\item Para classificar um registro:
		    \begin{enumerate}
		        \item Calcule a distância entre os registros;
		        \item Identifique os K vizinhos mais próximos;
		        \item Utilize o \emph{label} de classe dos vizinhos mais próximos para determinar a classe.
		    \end{enumerate}
	    \end{itemize}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Definição de KNN}
    \begin{definition}
        Os $k$ vizinhos mais próximos de um registro $x$ são os pontos que possuem as menores distâncias para $x$.
    \end{definition}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/knn-example02}
		\label{fig:knn-example02}
		\caption{Exemplo dos K-vizinhos mais próximos considerando: (a) 1 vizinho mais próximo; (b) 2 vizinhos mais próximos; (c) 3 vizinhos mais próximos \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Implementação}
    \begin{itemize}
        \item Para calcular a distância entre os pontos podemos utilizar a \alert{distância euclidiana} \cite{smu}:
        \[
            d(p, q) = \sqrt{\sum_i (p_i - q_i)^2}
        \]
        \item A classe é escolhida a partir da lista de vizinhos mais próximos:
        \begin{itemize}
            \item Voto de \alert{maioria} entre os vizinhos mais próximos (exemplo anterior);
            \item Cálculo de \alert{peso} relativo à distância:
            \[
             w = \frac{1}{d^2}
            \]
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Escolhendo bons valores de K}
    \begin{itemize}
        \item Se o valor de K for muito pequeno, o resultado é muito sensível a ruído;
        \item Se K for muito grande, a vizinhança pode incluir valores de outras classes;
        \item Regra de ouro: teste K valores entre 1 e 20;
        \item Importância do \alert{ajuste de hiper parâmetros}.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.35\textwidth]{../imagens/knn-example03}
		\label{fig:knn-example03}
		\caption{Exemplo dos K-vizinhos com pontos muito próximos e muito distantes \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Discussão sobre o KNN}
    \begin{minipage}[ht]{0.49\textwidth}
        \begin{itemize}
            \item Costuma trazer \alert{bons resultados}. Ex.: OCR -- \emph{Optical Character Recognition};
            \item Relativamente \alert{lento}, já que cada nova amostra precisa ser comparada com todos os exemplos anteriores.
        \end{itemize}
    \end{minipage}
    \begin{minipage}[ht]{0.49\textwidth}
        \includegraphics[width=.7\textwidth]{../imagens/knn-ocr}
    \end{minipage}
    \begin{itemize}
        \item Bons resultados dependem da escolha de uma \alert{boa medida de proximidade};
        \item KNN pode tratar fronteiras de decisão que \alert{não são paralelas aos eixos nem lineares};
        \item Precisa armazenar os dados de treinamento.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Fronteiras de decisão}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.7\textwidth]{../imagens/knn-fronteiras}
		\label{fig:knn-fronteiras}
		\caption{Fronteiras de decisão para um classificador 1-NN \cite{mannheim}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Problemas de escala}
    \begin{itemize}
        \item Alguns atributos talvez precisem escalar para impedir que as medidas de distância sejam dominadas por um único atributo \cite{smu};
        \item Exemplo:
        \begin{itemize}
            \item Tamanho de uma pessoa pode variar de 1,5m a 2,0m;
            \item Peso de uma pessoa pode variar de 50 a 150kg;
            \item Renda pode varia de R\$ 1mil a R\$1milhão. (tratar de renda pode dominar a distância euclidiana)
        \end{itemize}
        \item \alert{Solução}: padronização/escala usando z-score:
        \[
            Z = \frac{X-barX}{sd(X)}
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Lazy e eager learning}
    \begin{itemize}
        \item \alert{Lazy learning}:
        \begin{itemize}
            \item Classificadores baseados na instância (\emph{instance-based}) são chamados de \emph{lazy learning} porque nenhum modelo explícito é gerado;
            \item \underline{Objetivo único}: classificar novas amostras da maneira mais precisa possível;
            \item O KNN é um classificador do tipo \emph{lazy learning}.
        \end{itemize}
        \item \alert{Eager learning}:
        \begin{itemize}
            \item É possível ter \underline{dois objetivos}:
            \begin{enumerate}
                \item Classificar novas amostras;
                \item Entender o domínio da aplicação como um humano.
            \end{enumerate}
            \item \emph{Eager learning} gera modelos que podem ser interpretados por humanos;
            \item Exemplos de \emph{eager learning}: árvore de decisão, regras de associação.
        \end{itemize}
    \end{itemize}
\end{frame}


\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../ciencia-dados}
\bibliographystyle{apalike}

\end{document}
