\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}
\usepackage{multimedia}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Regras de associação}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Universidade Presbiteriana Mackenzie}

\date{\today} % Date, can be changed to a custom date
%\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}

\begin{frame}
    \frametitle{Regras de associação}
	\begin{itemize}
	    \item Exemplo clássico: dado um conjunto de transações, encontre regras que tentam predizer a ocorrência de um item baseado na ocorrência de outros na mesma transação;
	    \item Importante: \alert{coocorrência} não significa \alert{correlação}.
	\end{itemize}
	\bigskip
	\begin{minipage}[ht]{0.49\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=.8\textwidth]{../imagens/association-bread-milk}
			\label{fig:association-bread-milk}
			\caption{Exemplo de transações no carrinho de compras \cite{smu}}
	    \end{figure}
	\end{minipage}
	\begin{minipage}[ht]{0.49\textwidth}
	    \begin{itemize}
	        \item Algumas regras de associação possíveis: \\
	        \{Diaper\} $\rightarrow$ \{Beer\} \\
	        \{Milk, Bread\} $\rightarrow$ \{Eggs, Coke\} \\
	        \{Beer, Bread\} $\rightarrow$ \{Milk\}
        \end{itemize}
	\end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Problema exemplo}
    \begin{minipage}[ht]{0.39\textwidth}
        \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=.9\textwidth]{../imagens/ticket-data}
			\label{fig:ticket-data}
			\caption{Exemplo de transações de compra de ingresso \cite{ladeira}}
	    \end{figure}
    \end{minipage}
    \begin{minipage}[ht]{0.59\textwidth}
        \begin{itemize}
            \item O marketing tem acesso aos dados das cestas e compras (Figura \ref{fig:ticket-data}) e pretende estudar comportamento de compras;
            \item Exemplo de perguntas a responder:
            \begin{itemize}
                \item Que produtos estão associadas ao consumo de cerveja X?
                \item Como podemos descrever a população consumidora de amendoins?
                \item Onde devem estar localizados os produtos de limpeza doméstica ?
                \item Como se relacionam os produtos 1661 e 199?
            \end{itemize}
        \end{itemize}
    \end{minipage}
\end{frame}

\section{Construção das regras}

\begin{frame}
    \frametitle{Como expressar?}
    Como expressar a informação extraída?
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/expressando-informacao}
		\label{fig:expressando-informacao}
		\caption{Técnicas para expressar a informação \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Medidas de interesse}
    \begin{itemize}
        \item Regras de associação são expressadas através de métricas:
        \begin{itemize}
	        \item Tipicamente recorre-se a uma métrica de incidência para definir quais as associações significantes;
	        \item A mais popular é o \alert{suporte} (contagem) dos \emph{itemsets};
	        \item As regras são qualificadas por uma métrica de interesse (previsibilidade, solidez ou força da regra);
	        \item Normalmente é usada a \alert{confiança} (probabilidade condicional);
        \end{itemize}
        \item Assim, a regra de associação deve ser lida como \cite{ladeira}:
        \begin{quote}
		    \begin{figure}[ht!]
		        \centering
		        \includegraphics[width=.5\textwidth]{../imagens/regra-associacao}
				\label{fig:regra-associacao}
		    \end{figure}
            \begin{itemize}
                \item a compra conjunta dos produtos 901, 707 e 1088 ocorre em 30\% das transações. Por outro lado, verifica-se que 90\% das transações que contêm 901 e 707 também contêm o produto 1088; ou
                \item 90\% da sub-população definida pelos produtos 901 e 707 consomem 1088; ou
                \item a  contagem do consumo de 901 e 707, quando se adiciona 1088, desce para 90\% da inicial.
            \end{itemize}
        \end{quote}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Support}
    \begin{definition}
        Definimos o suporte $s$ (\emph{support}) do conjunto de itens (\emph{itemset}) $X$ como a fração das transações que contém o conjunto:
        \begin{equation}
            s(X) = \frac{\sigma(X)}{|T|}
            \label{eq:support}
        \end{equation}
        onde:
        \begin{itemize}
            \item $\sigma$ é a frequência de ocorrência de um \emph{itemset}. Ex.: $\sigma(\{Milk, Bread,Diaper\}) = 2$
            \item $T$ é o conjunto de itens nas transações e $|T|$ é o total de itens únicos.
        \end{itemize}
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Confiança e frequência}
    \begin{definition}
        A confiança (\emph{confidence}) $c$ mede  a frequência com que um item $Y$ aparece nas transações que contêm um item $X$:
        \begin{equation}
            c(X \rightarrow Y) = \frac{\sigma(X \cup Y)}{\sigma(X)} = \frac{s(X \cup Y)}{s(X)}
            \label{eq:confidence}
        \end{equation}
    \end{definition}
    \begin{definition}
        Um \alert{conjunto de itens frequente} (\emph{frequent itemset}) é um conjunto de itens cujo \emph{support} $s$ é maior ou igual a um \emph{threshold} $minsup$
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Regra de associação}
    \begin{definition}
        Uma regra de associação é da forma $X \rightarrow Y$ onde $X$ e $Y$ são conjuntos de itens e $X \cap Y = \emptyset$, ou seja, os conjuntos não contém itens em comum.
    \end{definition}
    Para a tabela \ref{fig:association-bread-milk2}, é possível calcular a regra $\{Milk, Diaper\} \rightarrow \{Beer\}$: \\
    \bigskip
    \begin{minipage}[ht]{0.49\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=.8\textwidth]{../imagens/association-bread-milk}
			\label{fig:association-bread-milk2}
			\caption{Exemplo de transações no carrinho de compras \cite{smu}}
	    \end{figure}
	\end{minipage}
	\begin{minipage}[ht]{0.49\textwidth}
	    \[
    	    \begin{split}
    	        s &= \frac{\sigma(Milk,Diaper,Beer)}{|T|} = \frac{2}{5} = 0.4 \\
    	        c &= \frac{\sigma(Milk,Diaper,Beer)}{\sigma(Milk,Diaper)} = \frac{2}{3} = 0.67
    	    \end{split}
	    \]
	\end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Objetivos da associação}
    \begin{itemize}
        \item Dado um conjunto de transações $T$, o objetivo da mineração de regras de associação é encontrar todas as regras com:
        \begin{itemize}
            \item $s(X) \geq minsup$
            \item $c(X \rightarrow Y) \geq minconf$
        \end{itemize}
        \item Abordagem da força bruta:
        \begin{itemize}
            \item liste todas as possíveis regras de associação;
            \item calcule o suporte e a confiança para cada regra;
            \item corte as regras que não satisfazem $minsup$ ou $minconf$.
        \end{itemize}
        \item Problema do \alert{custo computacional}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplo simples}
    \begin{itemize}
        \item O exemplo da figura \ref{fig:association-rules} contém apenas quatro itens (A, B, C e D);
        \item Para poucos exemplos foi possível gerar um total de \alert{quatorze regras}.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/association-rules}
		\label{fig:association-rules}
		\caption{Exemplo de regras de associação para quatro itens \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Mineração de regras de associação}
    \begin{itemize}
        \item Voltamos ao exemplo para calcular $s$ e $c$ para todas as transações da tabela;
        \begin{itemize}
            \item Todas as regras se referem aos mesmos itens $\{Milk, Diaper, Beer\}$;
            \item Regras originadas do mesmo \emph{itemset} têm o mesmo suporte mas podem ter confianças diferentes.
        \end{itemize}
        \item É possível separar \alert{suporte} e \alert{confiança}.
    \end{itemize}
    \begin{minipage}[ht]{0.42\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=.8\textwidth]{../imagens/association-bread-milk}
			\label{fig:association-bread-milk3}
			\caption{Exemplo de transações no carrinho de compras \cite{smu}}
	    \end{figure}
	\end{minipage}
	\begin{minipage}[ht]{0.56\textwidth}
    	\small
	    \[
    	    \begin{split}
    	        \{Milk,Diaper\} \rightarrow \{Beer\} & (s=0.4, c=0.67) \\
    	        \{Milk,Beer\} \rightarrow \{Diaper\} & (s=0.4, c=1.0) \\
    	        \{Diaper,Beer\} \rightarrow \{Milk\} & (s=0.4, c=0.67) \\
    	        \{Beer\} \rightarrow \{Milk,Diaper\} & (s=0.4, c=0.67) \\
    	        \{Diaper\} \rightarrow \{Milk,Beer\} & (s=0.4, c=0.5) \\
    	        \{Milk\} \rightarrow \{Diaper,Beer\} & (s=0.4, c=0.5)
    	    \end{split}
	    \]
	\end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Abordagem em dois passos}
    \begin{enumerate}
        \item \alert{Geração de itens frequentes}: gerar todos os \emph{itemsets} com suporte $\geq minsup$
        \item \alert{Geração das regras}: gerar regras de alta confiança para cada \emph{itemset}, onde cada regra é um partição binária de um \emph{itemset} frequente.
    \end{enumerate}
    \begin{itemize}
        \item Ainda assim a geração dos conjuntos de itens frequentes tem custo computacional proibitivo.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Geração dos conjuntos de itens frequentes}
    Dados $d$ itens, existem $2^d$ candidatos a conjuntos de dados possíveis.
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.7\textwidth]{../imagens/frequent-itemset}
		\label{fig:frequent-itemset}
		\caption{Exemplo de geração de conjuntos de itens frequentes \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Geração de itens frequentes -- força bruta}
    \begin{itemize}
        \item Cada itemset no reticulado (\emph{lattice}) é um conjunto frequente \alert{candidato};
        \item Calcule o suporte de cada candidato lendo o conjunto de dados;
        \item Tenta casar cada transação com um possível candidato;
        \item Complexidade: $\sim O(NM)$. \alert{Muito custoso!} (lembrando que $M=2^d$)
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.7\textwidth]{../imagens/bruteforce-itemset}
		\label{fig:bruteforce-itemset}
		\caption{Calculando o suporte para conjuntos frequentes candidatos \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Complexidade da força bruta}
    \begin{minipage}[ht]{0.49\textwidth}
        \begin{itemize}
            \item Considere $d$ itens únicos;
            \item Número total de \emph{itemsets}: $2^d$;
            \item Calculando o número total de possíveis regras de associação.
        \end{itemize}
        \bigskip
        \[
            \begin{split}
	            R &= \sum_{k=1}^{d-1} \left[ 
	                \left( \begin{array}{cc} d \\ k \end{array}  \right) \times
	                \sum_{j=1}^{d-k} \left( \begin{array}{cc} d-k \\ j \end{array} \right) 
	            \right] \\
	              & = 3^d - 2^{d+1} + 1
            \end{split}
        \]
    \end{minipage}
    \begin{minipage}[ht]{0.49\textwidth}
	    \begin{figure}[ht!]
	        \centering
	        \includegraphics[width=1\textwidth]{../imagens/bruteforce-complexity}
			\label{fig:bruteforce-complexity}
			\caption{Curva da complexidade \cite{smu}}
	    \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Estratégias para geração de itens frequentes}
    \begin{itemize}
        \item Reduzir o \alert{número de candidatos} (M)
        \begin{itemize}
            \item Faça a busca completa: $M = 2^d$;
            \item Usar técnicas de poda para reduzir M.
        \end{itemize}
        \item Reduzir o \alert{número de transações} (N)
        \begin{itemize}
            \item Reduzir o tamanho de N quando o número de itemsets aumenta;
            \item Usado pelo DHP e algoritmos baseados em mineração vertical.
        \end{itemize}
        \item Reduzir o \alert{número de comparações} (NM)
        \begin{itemize}
            \item Usar estruturas de dados eficientes para armazenar os candidatos ou as transações, sem necessidade de comparar cada candidato com cada transação.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Reduzindo o número de candidatos}
    \begin{itemize}
        \item Princípio do algoritmo \alert{Apriori}: se um itemset é frequente então todos os seus subconjuntos também são frequentes;
        \item O princípio se baseia nas propriedades de suporte descritas pela Equação \ref{eq:apriori}:
        \begin{equation}
            \forall X,Y: (X \cup Y) \rightarrow  s(X) \geq s(Y)
            \label{eq:apriori}
        \end{equation}
        \item O suporte de um itemset nunca é maior que o suporte de seus subconjuntos;
        \item É candidato se todos os seus subconjuntos são frequentes!
        \item Isto é conhecido como a propriedade \alert{anti-monotônica} do suporte.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Ilustrando o algoritmo Apriori}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.65\textwidth]{../imagens/apriori-principle}
		\label{fig:apriori-principle}
		\caption{Uma ilustração do corte de subconjuntos baseado no suporte. Se o conjunto $\{a, b\}$ não é frequente, então todos os subconjuntos de $\{a, b\}$ também não são frequentes. \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Corte com base no suporte}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/apriori-prunning}
		\label{fig:apriori-prunning}
		\caption{Diagrama ilustrando o corte baseado no suporte \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Algoritmo Apriori \cite{ladeira}}
    \begin{enumerate}
        \item[(1)] Dado um limiar de suporte \emph{minsup}, no primeiro passo encontre os itens que aparecem ao menos numa fração das transações igual a \emph{minsup}. Este conjunto é chamado $L_1$, dos itens frequentes;
        \item[(2)] Os pares dos itens em $L_1$ se tornam pares candidatos $C_2$ para o segundo passo. Os pares em $C_2$ cuja contagem alcançar \emph{minsup} são os pares frequentes $L_2$;
        \item[(3)] As triplas candidatas $C_3$ são aqueles conjuntos $\{A, B, C\}$ tais que todos os $\{A, B\}$, $\{A, C\}$ e $\{B, C\}$ estão em $L_2$. No terceiro passo, conte a ocorrência das triplas em $C_3$; aquelas cuja contagem alcançar \emph{minsup} são as triplas frequentes, $L_3$;
        \item[(4)] Proceda da mesma forma para tuplas de ordem mais elevada, até os conjuntos se tornarem vazios. $L_i$ são os conjuntos frequentes de tamanho $i$; $C_{i+1}$ é o conjunto de tamanho $i+1$ tal que cada subconjunto de tamanho $i$ está em $L_i$.
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Exemplo de descoberta das regras de associação}
    \textbf{Ex.:} Dada a tabela \ref{tab:associacao-exemplo} onde cada registro corresponde a uma transação de um cliente, com itens assumindo valores binários (sim/não), indicando se o cliente comprou ou não o respectivo item, descobrir todas as regras associativas com suporte $\geq$ 0,3 e grau de certeza (confiança) $\geq$ 0,8.
    \begin{table}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/associacao-exemplo}
		\label{tab:associacao-exemplo}
		\caption{Tabela exemplo de transações de compra \cite{ladeira}}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{Aplicando algoritmo ao exemplo}
    \begin{minipage}[ht]{0.48\textwidth}
    \small
    \[
        sup = \frac{\text{Número de registros com X e Y }}{\text{Número total de registros}} 
    \]
    \end{minipage}
    \begin{minipage}[ht]{0.48\textwidth}
    \small
    \[
        conf = \frac{\text{Número de registros com X e Y }}{\text{Número de registros com X}}
    \]
    \end{minipage}
    \bigskip
    \begin{enumerate}
        \item[(1)] Calcular o suporte de conjuntos com um item.
        \begin{itemize}
            \item Determinar os itens freqüentes com $sup \geq 0,3$
        \end{itemize}
        \item[(2)] Calcular o suporte de conjuntos com dois itens.
        \begin{itemize}
            \item Determinar conjuntos de itens freqüentes com $sup \geq 0,3$. \textbf{Obs.:} se um item não é frequente em (1), pode ser ignorado aqui.
            \item 	Descobrir as regras com alto fator de certeza.
        \end{itemize}
        \item[(3)] Calcular o suporte de conjuntos com três itens.
        \begin{itemize}
            \item Determinar conjuntos de itens freqüentes com $sup \geq 0,3$. \textbf{Obs.:} pelo mesmo motivo anterior, só é necessário se considerar conjuntos de itens que são freqüentes pelo passo anterior. 
            \item Descobrir regras com alto fator de  certeza.
        \end{itemize}
    \end{enumerate}
\end{frame}

\begin{frame}
    \frametitle{Aplicação das regras de associação}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/associacao-execucao}
		\label{fig:associacao-execucao}
		\caption{Aplicação do algoritmo Apriori \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Subconjuntos}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/associacao-execucao02}
		\label{fig:associacao-execucao02}
		\caption{Agrupação dos itens em conjuntos e seu respectivo suporte \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Regras candidatas com dois itens}
    \begin{figure}[ht!]
        \includegraphics[width=.5\textwidth]{../imagens/associacao-execucao03}
		\label{fig:associacao-execucao03}
		\caption{Regras candidatas com dois itens com o seu valor de certeza \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Regras candidatas com três itens}
    \begin{figure}[ht!]
        \includegraphics[width=.5\textwidth]{../imagens/associacao-execucao04}
		\label{fig:associacao-execucao04}
		\caption{Regras candidatas com três itens com o seu valor de certeza \cite{ladeira}}
    \end{figure}
\end{frame}

\section{Representação de conjuntos de itens}

\begin{frame}
    \frametitle{Conjunto de itens frequentes máximo}
    A frequência de um conjunto de itens é máxima se nenhum dos seus conjuntos imediatamente superiores é frequente.
    \begin{figure}[ht]
        \includegraphics[width=.7\textwidth]{../imagens/maximal-itemset}
		\label{fig:maximal-itemset}
		\caption{Maximal frequent itemset \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Conjunto de itens fechado}
    Um conjunto de itens é dito \alert{fechado} se nenhum de seus conjuntos imediatamente superiores tem o mesmo suporte (pode ter apenas suporte menor)
    \begin{figure}[ht]
        \includegraphics[width=.9\textwidth]{../imagens/closed-itemset}
		\label{fig:closed-itemset}
		\caption{Maximal frequent itemset \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Conjuntos de itens fechados x máximos}
    \begin{figure}[ht]
        \includegraphics[width=.9\textwidth]{../imagens/maximal-closed-itemsets}
		\label{fig:maximal-closed-itemsets}
		\caption{Comparativo entre conjuntos de itens fechados e máximos \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Frequência}
    \begin{figure}[ht]
        \includegraphics[width=.9\textwidth]{../imagens/max-closed-frequent-itemsets}
		\label{fig:max-closed-frequent-itemsets}
		\caption{Comparativo levando em consideração a frequência \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Diagramas de Venn dos itemsets}
    \begin{figure}[ht]
        \includegraphics[width=.55\textwidth]{../imagens/itemsets-conjunto}
		\label{fig:itemsets-conjunto}
		\caption{Relacionamento entre conjuntos de itens frequentes, fechados e máximos \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\section{Avaliação dos padrões}

\begin{frame}
    \frametitle{Avaliação deos padrões}
    \begin{itemize}
        \item Algoritmos de associação tendem a produzir \alert{regras demais};
        \item Muitas são \alert{desinteressantes} ou \alert{redundantes};
        \item Algumas medidas interessantes podem ser utilizadas para \alert{cortar/classificar} os padrões encontrados.
    \end{itemize}
    \begin{definition}
        Uma regra $\{A, B, C\} \rightarrow \{D\}$ pode ser considerada \alert{redundante} se $\{A, B\} \rightarrow \{D\}$ possui o mesmo valor de confiança ou maior.
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Exemplo de redundância}
    \begin{itemize}
        \item Regras redundantes contêm itens no antecedente que são explicados por outros itens também no antecedente \cite{ladeira};
        \item Exemplos:
        \begin{itemize}
	        \item Grávida $\rightarrow$ Mulher
	        \item Grávida \& Mulher $\rightarrow$ Retenção de líquidos
        \end{itemize}
        \item Descartar regra redundante $x \rightarrow y$ se $\exists z \in x: s(x \rightarrow y) = s(x-z \rightarrow y)$
    \end{itemize}
    \begin{figure}[ht]
        \includegraphics[width=.8\textwidth]{../imagens/regras-redundantes}
		\label{fig:regras-redundantes}
		\caption{Exemplo de regras redundantes \cite{ladeira}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Problema da confiança}
    \begin{figure}[ht]
        \includegraphics[width=.8\textwidth]{../imagens/confianca01}
		\label{fig:confianca01}
		\caption{Regra de associação: $Tea \rightarrow Coffee$ \cite{tan2016introduction}}
    \end{figure}
	\begin{itemize}
	    \item Confiança $\approxeq P(Coffee|Tea)$ = 15/20 = \alert{0,75};
	    \item A confiança é maior que 50\%, significando que pessoas que bebem chá têm maior chance de beber café do que de não beber;
	    \item Parece uma regra razoável. 
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Problema da confiança (cont.)}
    \begin{figure}[ht]
        \includegraphics[width=.4\textwidth]{../imagens/confianca02}
		\label{fig:confianca02}
		\caption{Regra de associação: $Tea \rightarrow Coffee$ \cite{tan2016introduction}}
    \end{figure}
	\begin{itemize}
	    \item Sabemos que Confiança $\approxeq P(Coffee|Tea)$ = 15/20 = \alert{0,75};
	    \item Observe contudo que $P(Coffee)$ = \alert{0,9}, ou seja, saber que uma pessoa bebe chá reduz a probabilidade dela beber café!
	    \item Note que $P(Coffee|\overline{Tea})$ = 75/80 = 0,9375, ou seja, a probabilidade de beber café sabendo que não bebe chá é maior do que a probabilidade de beber café dado que ela bebe chá.
	    \item É muito provável beber café de qualquer forma!
	\end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Quais métricas são importantes?}
    \begin{itemize}
        \item $Confianca(X \rightarrow Y)$ deve ser suficientemente grande para garantir que as pessoas que comprar $X$ têm maior chance de comprar $Y$ do que não comprar;
        \item $Confianca(X \rightarrow Y) > suporte(Y)$, caso contrário a regra estará levando a conclusões erradas, já que possuir $X$ na verdade reduz a chance de ter $Y$ na mesma transação;
        \item Qual é a melhor medida para capturar essas restrições?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Independência estatística}
    \begin{itemize}
        \item Considere uma população de 1000 estudantes;
        \begin{itemize}
	        \item 600 sabem nadar (S);
	        \item 700 sabem andar de bicicleta (B);
	        \item 450 sabem nadar e andar de bicicleta (S, B).
        \end{itemize}
        \item Podemos calcular então:
        \begin{itemize}
            \item $P(S, B) = \frac{450}{1000} = 0,45 \Rightarrow$ distribuição conjunta de probabilidade observada
            \item $P(S) \times P(B) = 0,6 \times 0.7 = 0,42 \Rightarrow$ valor esperado para variáveis independentes
        \end{itemize}
        \item É possível concluir então:
        \begin{itemize}
            \item $P(S, B) = P(S) \times P(B) \Rightarrow$ variáveis independentes
            \item $P(S, B) > P(S) \times P(B) \Rightarrow$ positivamente correlacionadas
            \item $P(S, B) < P(S) \times P(B) \Rightarrow$ negativamente correlacionadas
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Medidas estatísticas}
    A Figura mostra algumas medidas estatísticas que consideram a dependência estatística para a regra $X \rightarrow Y$
    \begin{figure}[ht]
        \includegraphics[width=.9\textwidth]{../imagens/medidas-estatisticas}
		\label{fig:medidas-estatisticas}
		\caption{Medidas estatísticas para dependência \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Medidas Lift}
    \begin{itemize}
        \item \alert{Suporte} e \alert{confiança} são usados como filtros para diminuir o número de regras geradas, gerando apenas regras de melhor qualidade;
        \item Consideremos então a regra \alert{A então B} com confiança de 90\%, podemos garantir que é interessante?
    \end{itemize}
    \begin{definition}
        A medida Lift é definida como:
        \begin{equation}
            Lift(A \rightarrow C) = \frac{conf(A \rightarrow C)}{s(C)}
            \label{eq:lift}
        \end{equation}
        onde $conf$ é a confiança e $s$ ś o suporte. 
    \end{definition}
\end{frame}

\begin{frame}
    \frametitle{Exemplo}
    \begin{itemize}
        \item Consideremos então duas regras $A \rightarrow B$ e $C \rightarrow D$:
        \begin{enumerate}
            \item [(1)] \alert{Se A então B} com confiança de 90\% NÃO é interessante se B apareceu em 90\% das transações, pois a regra não acrescentou nada em termos de conhecimento.
            \item[(2)] \alert{Se C então D}  com confiança de 70\%  á muito mais importante se D aparece, digamos, em 10\% das transações.
        \end{enumerate}
        \item Aplicamos o cálculo de Lift como definido na equação \ref{eq:lift}:
        \[
            Lift(A \rightarrow B) = \frac{conf(A \rightarrow B)}{s(B)} = \frac{0,9}{0,9} = 1 
        \]
        \[
            Lift(C \rightarrow D) = \frac{conf(C \rightarrow D)}{s(D)} = \frac{0,7}{0,1} = 7
        \]
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Voltando ao exemplo do café}
    \begin{figure}[ht]
        \includegraphics[width=.4\textwidth]{../imagens/confianca02}
		\label{fig:confianca03}
		\caption{Regra de associação: $Tea \rightarrow Coffee$ \cite{tan2016introduction}}
    \end{figure}
    \begin{itemize}
        \item Temos que Confiança = $P(Coffee|Tea)$ = 15/20 = \alert{0,75};
        \item Contudo, $P(Coffee) = 0,9$;
        \item Calculamos então o Lift:
        \[
            Lift = \frac{P(Coffee|Tea)}{P(Coffee)} = \frac{0,75}{0,9} = 0,833
        \]
        \item $Lift = 0,83333 < 1 \Rightarrow$ são negativamente correlacionados
        \item \alert{Não é suficiente} para afirmar que não tem correlação, mas \alert{já ajuda}.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Outras (muitas) medidas estatísticas}
    \begin{minipage}[ht]{0.29\textwidth}
        \begin{itemize}
            \item Existem \alert{muitas} outras medidas estatísticas;
            \item O melhor uso depende da \alert{aplicação};
            \item Deve considerar os \alert{objetivos de negócio};
            \item Muitas serão utilizadas ao longo do curso.
        \end{itemize}
    \end{minipage}
    \begin{minipage}[ht]{0.69\textwidth}
	    \begin{figure}[ht]
	        \includegraphics[width=.85\textwidth]{../imagens/statistical-measures}
			\label{fig:confianca03}
			\caption{Medidas estatísticas \cite{tan2016introduction}}
	    \end{figure}
    \end{minipage}
\end{frame}

\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../ciencia-dados}
\bibliographystyle{apalike}

\end{document}
