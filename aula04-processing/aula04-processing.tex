\documentclass{beamer}
\usepackage[brazil]{babel}
\usepackage[T1]{fontenc} %caracteres acentuados
\usepackage[utf8]{inputenc}
\usepackage{color,graphicx}
\usepackage{multirow}
\usepackage{multicol}
%\usepackage{caption} % <- no início do ficheiro !!! Vou comentar porque não funciona no beamer http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=388267
\usepackage{listings}
\usepackage{multimedia}

\lstset{
	numbers=left,
	stepnumber=5,
	firstnumber=1,
	numberstyle=\tiny,
	breaklines=true,
	frame=shadowbox,
	basicstyle=\tiny,
	stringstyle=\ttfamily,
	showstringspaces=false,
	extendedchars=\true,
	inputencoding=utf8,
	tabsize=2,
	%captionpos=bottom
}

% Beamer numbered captions
\setbeamertemplate{caption}[numbered]
\numberwithin{figure}{section}
\numberwithin{table}{section}

%\usetheme{CambridgeUS}
\mode<presentation>{\usetheme{AnnArbor}}
\definecolor{uofsgreen}{rgb}{.125,.5,.25}
%\usecolortheme[named=uofsgreen]{structure}
\setbeamertemplate{footline}[page number]
\AtBeginSection[]
{
\begin{frame}
\tableofcontents[currentsection]
\end{frame}
}

\AtBeginSubsection[]
{
\begin{frame}
\tableofcontents[currentsection,currentsubsection]
\end{frame}
}

\title{\textbf{Pré-processamento dos dados}}
\author{Eduardo Ferreira dos Santos}
\institute{Ciência da Computação \\ 
Universidade Presbiteriana Mackenzie}

\date{\today} % Date, can be changed to a custom date
%\logo{\includegraphics[scale=0.2]{../imagens/logo-ceub}}

\begin{document}
\begin{frame}
\titlepage
\end{frame}

\begin{frame}
	\frame{Sumário}
	\tableofcontents
\end{frame}



\begin{frame}
    \frametitle{Introdução}
    \begin{itemize}
        \item Construção do banco de dados final a partir dos dados brutos iniciais \cite{ladeira};
        \item Tarefas genéricas:
        \begin{itemize}
            \item seleção de atributos, limpeza, construção, integração e formatação dos dados de entrada.
        \end{itemize}
        \item Tarefas específicas:
        \begin{itemize}
            \item remoção de ruído ou de dados espúrios;
            \item estratégias para lidar com valores faltantes;
            \item formatação dos dados para a ferramenta a usar;
            \item criação de atributos derivados e de novos registros;
            \item integração de tabelas;
            \item discretização dos dados numéricos, se necessário.
        \end{itemize}        
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Bons modelos}
    \begin{itemize}
        \item Para a construção de bons modelos, os dados utilizados devem constituir:
        \begin{itemize}
            \item uma amostra aleatória (de preferência estratificada);
            \item representativa da população (domínio de aplicação) de interesse.
        \end{itemize}
        \item Problema do desbalanceamento:
        \begin{itemize}
            \item modelos melhores serão obtidos se artificialmente o desbalanceamento dos dados no conjunto de treinamento for reduzido;
            \item não se deve alterar as distribuições dos dados no conjunto de avaliação.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Por que pré-processar?}
    \begin{itemize}
        \item Dados reais são ``sujos'':
        \begin{description}
            \item[Incompletos] Falta de valores de atributos, falta de atributos de interesse ou existência de atributos agregados;
            \item[Ruidosos] Contém erros e desvios;
            \item[Inconsistentes] Contém discrepâncias em nomes e na codificação.
        \end{description}
        \item Resultados de qualidade requerem dados de qualidade.
        \item \alert{Garbage in, garbage out!}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Medidas de qualidade}
    \begin{itemize}
        \item Correção;
        \item Completude;
        \item Consistência;
        \item Atualidade;
        \item Credibilidade;
        \item Valor adicionado;
        \item Interpretabilidade;
        \item Acessibilidade.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Por que os dados são sujos?}
    \begin{itemize}
        \item Dados incompletos têm origem em \cite{ladeira}:
        \begin{itemize}
            \item Indisponibilidade durante a coleta;
            \item Considerações diferentes quando o dado foi coletado e quando foi analisado;
            \item Falha humana/software/hardware.
        \end{itemize}
        \item Dados ruidosos têm origem no processo de:
        \begin{itemize}
            \item Coleta;
            \item Entrada;
            \item Transmissão.
        \end{itemize}
        \item Dados inconsistentes têm origem em:
        \begin{itemize}
            \item Fontes diferentes;
            \item Violação de dependências funcionais.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Principais tarefas}
    \begin{itemize}
        \item \alert{Limpeza}: preenchimento de valores ausentes, atenuação de dados ruidosos, resolução de inconsistências;
        \item \alert{Agregação}: integração de múltiplos DB e arquivos;
        \item \alert{Amostragem}: obtenção de uma amostra menor do conjunto de dados para testar os modelos;
        \item \alert{Discretização e binarização}: conversão de atributos numéricos em categóricos ou binários;
        \item \alert{Redução}: obtenção de uma representação reduzida em volume mas que produz resultados de análise idênticos ou similares.
    \end{itemize}
\end{frame}

\section{Limpeza}

\begin{frame}
    \frametitle{Limpeza dos dados}
    \begin{itemize}
        \item Padronização:
        \begin{itemize}
            \item Formatos de dados;
            \item Abreviaturas;
            \item Valores de atributos (diversas representações de sexo);
            \item Normalização de valores numéricos:
            \begin{itemize}
                \item Para intervalo [0, 1]
                \[
                    \frac{x - min}{max-min}
                \]
                \item Normal padrão (z-score)
                \[
                    \frac{x-\text{média}}{\text{desvio padrão}}
                \]
            \end{itemize}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Tratamento de ruídos}
    \begin{itemize}
        \item Os dados com ruído não são úteis, pois não podem ser interpretados pelas máquinas;
        \item Os ruídos em geral \alert{acontecem} e temos que aprender a lidar com eles;
        \item Algumas técnicas para tratar os ruídos:
        \begin{itemize}
            \item Método de Binning: trabalha em dados ordenados para torná-los mais suaves;
            \begin{itemize}
                \item Os dados são divididos em segmentos de igual tamanho;
                \item Os segmentos de melhor qualidade são utilizados para substituir os valores com ruído;
                \item Podem se utilizada a média ou outros valores na fronteira entre os segmentos.
            \end{itemize}
            \item Regressão: tenta suavizar os dados aplicando uma função de regressão;
            \item Clustering: agrupa os dados similares e tenta remover os \emph{outliers}, possivelmente representando o ruído.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Valores ausentes}
    \begin{itemize}
        \item Duas estratégias principais para lidar com valores ausentes: imputar ou remover os dados;
        \item Imputação requer encontrar uma estimativa razoável para os dados ausentes:
        \begin{itemize}
            \item Útil quando o percentual de dados ausentes é baixo;
            \item Se o percentual for muito grande os dados não vão possuir a variabilidade necessária para produzir um modelo efetivo.
        \end{itemize}
        \item Se os dados estiverem ausentes em instâncias aleatórias a remoção pode ser feita com segurança;
        \item Pergunta relevante: por que os dados estão ausentes?
            \begin{itemize}
		        \item Missing at Random (MAR): os dados estão ausentes em relação aos dados observados, mas não em todas as instâncias;
		        \item Missing Completely at Random (MCAR): dados ausentes em todas as instâncias;
		        \item Missing Not at Random (MNAR): aplicado quando os dados estão ausentes de maneira estruturada. Ex.: determinado grupo etário não respondeu ao questionário.
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Valores duplicados}
    \begin{itemize}
        \item Os conjuntos de dados podem contem objetos que são duplicados ou muito similares. Ex.: mesma pessoa com múltiplos e-mails;
        \item O maior problema quando são mesclados dados de diferentes fontes;
        \item A maior parte do processo de limpeza é remover os dados duplicados;
        \item Quando não remover dados duplicados?
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Medidas de similaridade e dissimilaridade}
    \begin{itemize}
        \item Medida de similaridade:
        \begin{itemize}
            \item Medida numérica do quão diferentes são dois objetos de dado;
            \item Maior quando os objetos são mais parecidos;
            \item Frequentemente está entre os valores [0,1].
        \end{itemize}
        \item Medida de dissimilaridade:
        \begin{itemize}
            \item Medida numérica da diferença entre dois objetos de dado;
            \item Menor quando os objetos são mais parecidos;
            \item Dissimilaridade mínima é zero frequentemente;
            \item Limite superior varia.
        \end{itemize}
        \item A medida de \alert{proximidade} se refere a similaridade ou dissimilaridade.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Proximidade em atributos simples}
    A tabela mostra a similaridade e dissimilaridade entre os objetos $x$ e $y$ com respeito a um único atributo.
    \begin{table}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/similaridade}
		\label{tab:similaridade}
		\caption{Medida de similaridade e dissimilaridade entre dois objetos $x$ e $y$ \cite{tan2016introduction}}
    \end{table}
\end{frame}

\begin{frame}
    \frametitle{Distância euclidiana}
    \begin{definition}
        A distância euclidiana é da forma
        \[
            d(x,y) = \sqrt[2]{\sum^{n}_{k=1}(x_k - y_k)^2}
        \]
        onde $n$ é o número de dimensões (atributos) e $x_k$ e $y_k$ são, respectivamente, os k-ésimos atributos (componentes) ou objetos de dados $x$ e $y$.
    \end{definition}
    \begin{itemize}
        \item A padronização pode ser necessária caso as escalas sejam diferentes.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Distância euclidiana -- exemplo}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/distancias}
		\label{fig:distancias}
		\caption{Distância euclidiana entre os pontos $p_1$, $p_2$, $p_3$ e $p_4$ \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Distância de Minkowski}
    \begin{block}{Distância de Minkowski}
        A distância de Minkowski é a generalização da distância euclidiana
        \[
            d(x,y) = \left(\sum^{n}_{k=1}|x_k - y_k|^r \right)^{1/r}
        \]
        onde $r$ é um parâmetro, $n$ é o número de dimensões (atributos) e $x_k$ e $y_k$ são, respectivamente, os k-ésimos atributos (componentes) ou objetos de dados $x$ e $y$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Distância de Minkowski -- exemplo}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/minkowski}
		\label{fig:minkowski}
		\caption{Distância de Minkowski entre os pontos $p_1$, $p_2$, $p_3$ e $p_4$ \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Propriedades comuns entre as distâncias}
    \begin{itemize}
        \item Distâncias, tais como a distância euclidiana, possuem algumas propriedades bem conhecidas;
        \item Uma distância que satisfaz as propriedades é uma \alert{métrica}.
    \end{itemize}
    \bigskip
    \begin{enumerate}
        \item $d(x, y) \geq 0$ para todo $x$ e $y$, e $d(x, y) = 0$ somente se $x=y$;
        \item $d(x, y) = d(y, x)$ para todo $x$ e $y$ (Simetria);
        \item $d(x, z) \leq d(x, y) + d(y, z)$ para todos os pontos $x$, $y$ e $z$ (desigualdade de triângulos).
    \end{enumerate}
    \bigskip
    onde $d(x, y)$ é a distância (dissimilaridade) entre pontos (objetos de dado), $x$ e $y$.
\end{frame}

\begin{frame}
    \frametitle{Propriedades comuns de similaridade}
    \begin{itemize}
        \item As similaridades também possuem propriedades conhecidas.
    \end{itemize}
    \bigskip
    \begin{enumerate}
        \item $s(x, y) = 1$ (ou similaridade máxima) somente se $x=y$;
        \item $s(x, y) = s(y, x)$ para todo $x$ e $y$ (simetria).
    \end{enumerate}
    \bigskip
    onde $s(x, y)$ é a similaridade entre pontos (objetos de dado), $x$ e $y$.
\end{frame}

\begin{frame}
    \frametitle{Similaridade entre vetores binários}
    \begin{itemize}
        \item É comum que os objetos de dado $x$ e $y$ possuam apenas atributos binários;
        \item Calcula as similaridades utilizando as seguintes quantidades:
        \begin{itemize}
            \item $f_{01}$ = número de atributos onde $x$ foi 0 e $y$ foi 1;
            \item $f_{10}$ = número de atributos onde $x$ foi 1 e $y$ foi 0;
            \item $f_{00}$ = número de atributos onde $x$ foi 0 e $y$ foi 0;
            \item $f_{11}$ = número de atributos onde $x$ foi 1 e $y$ foi 1.
        \end{itemize}
        \item \emph{Simple match} e coeficiente de Jaccard
    \end{itemize}
    \[
        \begin{split}
            SMC &= \text{número de matches} / \text{número de atributos} \\
             & = (f_{11} + f_{00}) / (f_{01} + f_{10} + f_{11} + f_{00}) \\
            J &= \text{número de 11 matches} / \text{número de atributos diferentes de zero} \\
             & = (f_{11}) / (f_{01} + f_{10} + f_{11})
        \end{split}
    \]
\end{frame}

\begin{frame}
    \frametitle{Exemplo: SMC vs Jaccard}
    \[
        \begin{split}
            x & = 1 0 0 0 0 0 0 0 0 0 \\
            y & =  0 0 0 0 0 0 1 0 0 1 \\
            f_{01} & = 2 \text{ número de atributos onde x foi 0 e y foi 1} \\
            f_{10} & = 1 \text{ número de atributos onde x foi 1 e y foi 0} \\
            f_{00} & = 7 \text{ número de atributos onde x foi 0 e y foi 0} \\
            f_{11} & = 0 \text{ número de atributos onde x foi 1 e y foi 1} \\
            SMC & = (f_{11} + f_{00}) / (f_{01} + f_{10} + f_{11} + f_{00}) \\
             & = (0+7) / (2+1+0+7) = 0.7  \\
            J & = (f_{11}) / (f_{01} + f_{10} + f_{11}) \\
             & = 0 / (2 + 1 + 0) = 0 
        \end{split}
    \]
\end{frame}

\begin{frame}
    \frametitle{Estatística}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.7\textwidth]{../imagens/correlacao}
		\label{fig:correlacao}
		\caption{Medidas estatísticas básicas \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Avaliando a correlação de maneira visual}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.6\textwidth]{../imagens/correlacao-exemplo}
		\label{fig:correlacao-exemplo}
		\caption{Gráfico de dispersão mostrando a similaridade de -1 para 1 \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Problema da correlação}
    \begin{figure}[ht!]
        \begin{minipage}[ht]{0.49\textwidth}
            \begin{itemize}
                \item x = (-3, -2, -1, 0, 1, 2, 3)
                \item y = (9, 4, 1, 0, 1, 4, 9)
                \item $y_i = x_{i}^2$
                \item mean(x) = 0, mean(y) = 4
                \item std(x) = 2.16, std(y) = 3.74
            \end{itemize}
        \end{minipage}
        \begin{minipage}[ht]{0.49\textwidth}
            \centering
            \includegraphics[width=1\textwidth]{../imagens/correlacao-grafico}
            \bigskip
        \end{minipage}
        corr = (-3)(5)+(-2)(0)+(-1)(-3)+(0)(-4)+(1)(-3)+(2)(0)+3(5) / ( 6 * 2.16 * 3.74 ) = 0
		\label{fig:correlacao-grafico}
		\caption{Exemplo de problemas na correlação \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Medidas de proximidade}
    \begin{itemize}
        \item A escolha da medida de proximidade mais correta depende do domínio;
        \item Qual é a melhor escolha da medida de proximidade para as seguintes situações?
        \begin{itemize}
            \item Comparando documentos utilizando a frequência das palavras: os documentos são considerados similares se a frequência das palavras é similar;
            \item Comparando a temperatura em Celsius em duas localizações: são consideradas similares se as temperaturas são similares em magnitude;
            \item Comparando duas séries temporais de temperatura medida em Celsius: duas séries temporais são consideradas similares se possuem o ``formato'' similar.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Comparativo de medidas de proximidade}
    \begin{itemize}
        \item Domínio da aplicação:
        \begin{itemize}
            \item Medidas de similaridade tendem a ser específicas de acordo com o tipo de atributos e dados;
            \item Registros numéricos, imagens, gráficos, sequências, etc, tendem a possuir diferentes medidas.
        \end{itemize}
        \item Existem várias propriedades interessantes para uma medida de proximidade:
        \begin{itemize}
            \item Simetria;
            \item Tolerância a ruídos e \emph{outliers};
            \item Capacidade de encontrar padrões;
            \item etc.
        \end{itemize}
        \item A medida deve ser aplicável aos dados e produzir resultados de acordo com o domínio de conhecimento.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Medidas baseadas em informação}
    \begin{itemize}
        \item A teoria da informação é uma disciplina bem desenvolvida e fundamental com muitas aplicações;
        \item Algumas medidas de similaridade são baseadas na teoria da informação:
        \item A informação está relacionada às possíveis saídas de um evento: transmissão de uma mensagem, virar uma moeda, etc
        \item Quanto mais certa a saída menos informação ela contém, e vice-cersa:
        \begin{itemize}
            \item Lançar uma moeda só tem duas possibilidades, então girar uma coroa não traz muita informação;
            \item De maneira quantitativa, a saída está relacionada à sua probabilidade.
        \end{itemize}
        \item Medida comum: entropia.
    \end{itemize}
\end{frame}

\section{Agregação}

\begin{frame}
    \frametitle{Integração dos dados}
    \begin{itemize}
        \item Combina dados de diferentes fontes em uma armazenagem única e coerente;
        \item Detecta e resolve conflito de valores:
        \begin{itemize}
            \item Integração de esquemas de BD: problema da identificação de entidades (modelo conceitual)
            \item Eliminação de redundâncias (dicionário de dados);
            \item Detecção e resolução de valores conflitantes nos dados.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Objetivos}
    \begin{description}
        \item[Redução dos dados] Reduzir o número de atributos ou objetos;
        \item[Mudança de escala] Cidades agregadas em regiões, estados, países, etc;
        \item[Estabilidade] Dados agregados tendem a possuir menor variabilidade (ex.: mudar a escala de tempo de mensal para anual)
    \end{description}
\end{frame}

\begin{frame}
    \frametitle{Exemplo de agregação}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/agregation-example}
		\label{fig:agregation-example}
		\caption{Precipitação na Austrália \cite{smu}}
    \end{figure}
\end{frame}

\section{Amostragem}

\begin{frame}
    \frametitle{Seleção de dados}
    \begin{itemize}
        \item \alert{Amostragem} é técnica mais utilizada para seleção de dados \cite{smu};
        \item É bastante usada tanto para investigação preliminar dos dados como para análise final;
        \item Pode ser muito custoso \alert{obter} todo o conjunto de dados;
        \item A amostragem é utilizada também porque \alert{processar} todo o conjunto de dados pode ser muito custoso;
        \item Alguns princípios básicos para amostragem:
        \begin{itemize}
            \item A amostra precisa ser \alert{representativa} para garantir que o modelo vai funcionar como esperado;
            \item Uma amostra é representativa se tiver as mesmas características de interesse do conjunto de dados original.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Tipos de amostragem}
    \begin{itemize}
        \item Substituição:
        \begin{itemize}
	        \item Amostragem com substituição: à medida que uma instância é selecionada ela é removida da população;
	        \item Amostragem sem substituição: as instâncias não são removidas da população quando são selecionadas para a amostra.
        \end{itemize}
        \item Seleção:
        \begin{itemize}
            \item Amostragem aleatória simples: existe uma probabilidade de seleção igual para todas as instâncias;
            \item Amostragem estratificada: divide os dados em múltiplas partições e divide as partições em amostras aleatórias.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Tamanho da amostra}
    \begin{itemize}
        \item Definir o tamanho ideal para a amostra poder ser um desafio;
        \item No exemplo da Figura \ref{fig:sample}, quantos pontos são necessários para identificar o conteúdo da imagem?
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=1\textwidth]{../imagens/sample}
		\label{fig:sample}
		\caption{Amostra de pixels para a imagem \cite{smu}}
    \end{figure}
\end{frame}


\section{Discretização e binarização}

\begin{frame}
    \frametitle{Discretização}
    \begin{itemize}
        \item Discretização é o processo de converter um atributo contínuo em um atributo ordinal;    
        \item Um número potencialmente infinito de valores é mapeado em um conjunto menor de categorias;
        \item Discretização é utilizada tanto em configurações supervisionadas como não supervisionadas.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Discretização não supervisionada}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/discretizacao}
		\label{fig:discretizacao}
		\caption{Exemplo de discretização não supervisionada \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Intervalo igual}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/discretizacao02}
		\label{fig:discretizacao02}
		\caption{Abordagem de intervalo igual para 4 valores \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Frequência igual}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/discretizacao03}
		\label{fig:discretizacao03}
		\caption{Abordagem de frequência igual para 4 valores \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{K-means}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/discretizacao04}
		\label{fig:discretizacao04}
		\caption{Abordagem de K-means para obter 4 valores \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Discretização supervisionada}
    \begin{itemize}
        \item Muitos algoritmos de classificação funcionam melhor se as variáveis dependentes e independentes possuem somente alguns valores;
        \item A Figura mostra um exemplo da utilidade da discretização.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.7\textwidth]{../imagens/discretizacao-supervisionada}
		\label{fig:discretizacao-supervisionada}
		\caption{Abordagem de K-means para obter 4 valores \cite{tan2016introduction}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Binarização}
    \begin{itemize}
        \item A binarização mapeia um atributo contínuo ou categórico em uma ou mais variáveis binárias.
    \end{itemize}
    \begin{table}[ht!]
        \centering
        \includegraphics[width=.8\textwidth]{../imagens/binarizacao}
		\label{fig:binarizacao}
		\caption{Exemplo de conversão de atributos categóricos \cite{tan2016introduction}}
    \end{table}
\end{frame}


\section{Redução}

\begin{frame}
    \frametitle{Redução de dados}
    \begin{itemize}
        \item Objetiva obter uma representação reduzida das instâncias que é muito menor no volume mas produz os mesmos (ou quase os mesmos) resultados analíticos
        \item Técnicas:
        \begin{itemize}
            \item Redução de dimensionalidade;
            \item Compressão de dados;
            \item Criação de atributos.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Maldição da dimensionalidade}
    \begin{figure}[ht!]    
	    \begin{minipage}[ht]{0.49\textwidth}
	        \begin{itemize}
	            \item Quando a dimensionalidade dos dados aumenta, o espaço que eles ocupam aumenta exponencialmente;
	            \item Algumas definições, como densidade e distância entre os pontos, passam a ter menos significado:
	            \begin{itemize}
	                \item Densidade $\rightarrow$ 0;
	                \item Todos os pontos tendem a possuir a mesma distância euclidiana entre eles.
	            \end{itemize}
	        \end{itemize}
		    \bigskip
	    \end{minipage}
	    \begin{minipage}[ht]{0.49\textwidth}
		        \centering
		        \includegraphics[width=.8\textwidth]{../imagens/dimensionality}
	    \end{minipage}
		\label{fig:dimensionality}
		\caption{Número de dimensões e distância entre os pontos \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Redução de dimensionalidade}
    \begin{itemize}
        \item Objetivos \cite{smu}:
	        \begin{itemize}
	            \item Evitar a maldição da dimensionalidade;
		        \item Permitir que os dados sejam visualizados mais facilmente;
		        \item Pode ajudar a eliminar \emph{features} que não sejam relevantes ou reduzir ruído.
	        \end{itemize}
        \item Técnicas:
        \begin{itemize}
            \item Principal Component Analysis (PCA);
            \item Singular Value Decomposition (SVD);
            \item Outras técnicas supervisionadas e não lineares.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Singular Value Decomposition -- SVD}
    O SVD decompõe $M$ em três ações simples: rotação \textbf{$V^*$}, escala (esticamento) \textbf{$\Sigma$} e uma segunda rotação \textbf{U}.
    \bigskip
    \begin{figure}[ht!]
        \begin{minipage}[ht]{0.49\textwidth}
            \centering
            \includegraphics[width=1\textwidth]{../imagens/svd}
        \end{minipage}
        \begin{minipage}[ht]{0.49\textwidth}
            \begin{itemize}
                \item \alert{Topo}: dois vetores canônicos $e_1$ e $e_2$, sofrendo a ação \textbf{M}, que distorce o círculo a uma elipse;
                \item \alert{Esquerda}: rotação \textbf{$V^*$} aplicada em $e_1$ e $e_2$;
                \item \alert{Direita}: rotação \textbf{U};
                \item \alert{Baixo}: esticamento \textbf{$\Sigma$} medido pela escala $\sigma_1$ (horizontal) e $\sigma_2$ (vertical) conhecidos como \alert{valores singulares}.
            \end{itemize}
        \end{minipage}
		\label{fig:svd}
		\caption{Passos da decomposição SVD \cite{wiki:svd}}
    \end{figure}
\end{frame}


\begin{frame}
    \frametitle{Principal Component Analysis -- PCA}
    \begin{itemize}
        \item \alert{Objetivo}: mapear os pontos para um espaço de menor dimensão preservando a informação da distância entre eles;
        \item \alert{Método}: encontrar uma projeção (novos eixos) que capturam a maior quantidade de variação nos dados;
        \item Pode ser feito utilizando autovetores da matriz de covariância ou SVD.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/pca}
		\label{fig:pca}
		\caption{Exemplo de aplicação do PCA \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{ISOMAP}
    \begin{itemize}
        \item \alert{Objetivo}: desenrolar o bolo (preservar as distâncias no rolo);
        \item \alert{Método}: utilizar um espaço não métrico, ou seja, as distâncias não são medidas pela distância euclidiana, mas ao longo da superfície do rolo (distância geodésica):
        \begin{enumerate}
            \item Construa um gráfico de vizinhos (\textit{k-nearest neighbors} ou um raio);
            \item Para cada ponto no gráfico calcule o menor caminho para as distâncias = distâncias geodésicas;
            \item Crie um conjunto de atributos de menor dimensão (\textit{low-dimensional embedding}) utilizando as distâncias geodésicas.
        \end{enumerate}
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/isomap}
		\label{fig:isomap}
		\caption{Método ISOMAP \cite{smu}}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Low-dimensional embedding}
    \begin{itemize}
        \item Noção geral de representação de objetos descritos e um espaço (ou sja, um conjunto de atributos) em um diferente espaço aplicando o mapeamento $f : X \rightarrow Y$;
        \item O PCA é um exemplo onde o espaço $Y$ é medido pelos componentes principais e a proximidade de objetos no espaço original $X$ são embutidos (\textit{embedded}) no espaço $Y$;
        \item \textit{Low-dimensional embeddings} podem ser produzidos utilizando vários métodos:
        \begin{itemize}
            \item T-SNA -- T-distributed Stochastic Neighbor Embedding: visualização não linear de conjuntos de dados de muitas dimensões;
            \item \textit{Autoencoders (deep learning)}: não linear;
            \item \textit{Word embedding}: Word2vec, GloVe, BERT.
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Exemplos de embeddings}
    \begin{itemize}
        \item No caso de \emph{word embeddings} a redução de dimensionalidade auxilia na variabilidade da língua;
        \item Redes neurais auxiliam a tratar o número elevado de features de entrada.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=.9\textwidth]{../imagens/embedding}
		\label{fig:embedding}
		\caption{Embeddings, encoders e redução de dimensionalidade \cite{smu}}
    \end{figure}
\end{frame}


\begin{frame}
    \frametitle{Seleção de atributos (\emph{feature selection})}
    \begin{itemize}
        \item Outra técnica para reduzir a dimensionalidade;
        \item Atributos redundantes:
        \begin{itemize}
            \item Duplicam parte ou toda a informação contida em outros atributos (estão correlacionados);
            \item Ex.: preço de compra de um produto ou o total de impostos pagos.
        \end{itemize}
        \item Atributos irrelevantes:
        \begin{itemize}
            \item Contém pouca ou nenhuma informação que seja útil para a tarefa de mineração de dados;
            \item Ex.: ID do estudante dificilmente será relevante para a tarefa de predizer a nota na prova.
        \end{itemize}
        \item Abordagens embutidas: a seleção de atributos acontece como parte do algoritmo de mineração de dados (ex.: regressão, árvores de decisão)
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Seleção de atributos (\emph{feature selection}) (2)}
    \begin{itemize}
        \item Abordagem de filtragem:
        \begin{itemize}
            \item Os atributos são selecionados antes de executar o algoritmo;
            \item Ex.: atributos com alta correlação.
        \end{itemize}
        \item Abordagem de força bruta: fornece aos algoritmos de mineração todos os subconjuntos de atributos e escolhe os que obtém melhores resultados;
        \item Abordagens de empacotamento: utilizam os algoritmos de mineração de dados como uma caixa preta para encontrar o melhor subconjunto de atributos.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Criação de atributos}
    \begin{itemize}
        \item Criar atributos que capturem as informações importantes de um conjunto de dados de maneira mais eficiente que os atributos originais;
        \item Três principais metodologias:
        \begin{itemize}
            \item Extração de atributos (\emph{Feature extraction}): normalmente específica do domínio (ex.: reconhecimento facial em processamento de imagens)
            \item Construção/Engenharia de atributos (\emph{Feature engineering}): combinação de atributos
            \item Mapear os dados para novos espaços:
            \begin{itemize}
                \item Transformada de Fourier;
                \item Transformada Wavelet.
            \end{itemize}
        \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Mapeando dados a novos espaços}
    \begin{itemize}
        \item Transformada de Fourier;
        \item Transformada Wavelet.
    \end{itemize}
    \begin{figure}[ht!]
        \centering
        \includegraphics[width=1\textwidth]{../imagens/new-space}
		\label{fig:new-space}
		\caption{Exemplo de mudança de espaço \cite{smu}}
    \end{figure}
\end{frame}


\begin{frame}
    \frametitle{Compressão de dados}
    \begin{itemize}
        \item Contador de instâncias repetidas (lido como um novo atributo)
        \item Substituição de símbolos por números
        \begin{itemize}
            \item Arquivo com cabeçalho de símbolos;
            \item Dados codificados com símbolos substituídos por números.
        \end{itemize}
    \end{itemize}
\end{frame}





\begin{frame}
	\begin{center}
		\textbf{OBRIGADO!!!} \\
		\textbf{PERGUNTAS???}
	\end{center}
\end{frame}

\bibliography{../ciencia-dados}
\bibliographystyle{apalike}

\end{document}
